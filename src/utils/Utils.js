import React from "react";
import namor from "namor";
import jsonData from './data.json';

const range = len => {
  const arr = [];
  for (let i = 0; i < len; i++) {
    arr.push(i);
  }
  return arr;
};

const newPerson = () => {
  const statusChance = Math.random();
  return {
    firstName: namor.generate({ words: 1, numbers: 0 }),
    lastName: namor.generate({ words: 1, numbers: 0 }),
    age: Math.floor(Math.random() * 30)
  };
};

export function makeData(len) {
  return range(len).map(d => {
    return {
      ...newPerson()
    };
  });
}
