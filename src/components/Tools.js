import React, { Component } from 'react';
import "react-table/react-table.css";
import Chance from "chance";
import './Tools.css';

const chance = new Chance();

class Tools extends Component {

	constructor(props) {
		console.log("Tools, constructor props", props);
		super(props);
		const statusChance = Math.random();
		this.state = {
			firstName: '',
			lastName: '',
			age: ''
		};
		this.handleInputChange = this.handleInputChange.bind(this);
		this.handleSubmit = this.handleSubmit.bind(this);
	}

	componentDidMount(props) {
		if(props !== undefined) {
		this.setState({ data: this.props.data });
		}
	}

	componentWillReceiveProps(props) {
		if(props.data !== undefined) {
			this.setState({ data: props.data });
		}
	}

	handleInputChange(event) {
		const target = event.target;
		const value = target.value;
		const name = target.name;

		this.setState({
			[name]: value
		});
	};

	newPerson() {
		return {
			_id: chance.guid(),
			firstName: this.state.firstName,
			lastName: this.state.lastName,
			age: this.state.age
		};
	};

	handleSubmit (event) {
		event.preventDefault();
		this.props.dataUpdater(this.newPerson())
	};  

	render() {
		const enabled = this.state.firstName !== "" && this.state.lastName !== "" && this.state.age !== "";
		return (
			<div className="InputForm">
				<i>Admin</i>
				<h2>Add new person</h2>
				<form onSubmit={this.handleSubmit}>
					<div class="form-group">
						<label for="firstName">First name</label>
						<input class="form-control" id="firstName"
							name="firstName"
							type="text"
							value={this.state.firstName}
							onChange={this.handleInputChange} />
					</div>
					<br />
					<div class="form-group">
					<label for="lastName">Last name</label>
						<input class="form-control" id="lastName"
							name="lastName"
							type="text"
							value={this.state.lastName}
							onChange={this.handleInputChange} />
					</div>
					<br />
					<div class="form-group">
						<label for="age">Age</label>
						<input class="form-control" id="age"
							name="age"
							type="number"
							value={this.state.age}
							onChange={this.handleInputChange} />
					</div>
					<br />
					<input type="submit" value="Submit" className="btn btn-primary" disabled={!enabled}/>
				</form>
			</div>
		);
	}
}

export default Tools;
