import React from "react";
import './Table.css';
import matchSorter from 'match-sorter';

import ReactTable from "react-table";
import checkboxHOC from "react-table/lib/hoc/selectTable";
import "react-table/react-table.css";

const CheckboxTable = checkboxHOC(ReactTable);

function getColumns(data) {
	const columns = [];
	const sample = data[0];
	Object.keys(sample).forEach(key => {
		if (key !== "_id") {
			columns.push({
				accessor: key,
				Header: key
			});
		}
	});
	return columns;
}

class Table extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			...props,
			data: [],
			selection: [],
			columns: [],
			selectAll: false
		};
	}

	componentDidMount(props) {
		if(this.props !== undefined) {
		this.setState({ data: this.props.data });
		}
	}

	componentWillReceiveProps(props) {
		if(props.data !== undefined) {
			this.setState({ data: props.data });
		}
	}

 toggleSelection = (key, shift, row) => {
		let selection = [...this.state.selection];
		const keyIndex = selection.indexOf(key);
		if (keyIndex >= 0) {
			selection = [
				...selection.slice(0, keyIndex),
				...selection.slice(keyIndex + 1)
			];
		} else {
			selection.push(key);
		}
		this.setState({ selection });
	};

	toggleAll = () => {
		const selectAll = this.state.selectAll ? false : true;
		const selection = [];
		if (selectAll) {
			const wrappedInstance = this.checkboxTable.getWrappedInstance();
			const currentRecords = wrappedInstance.getResolvedState().sortedData;
			currentRecords.forEach(item => {
				selection.push(item._original._id);
			});
		}
		this.setState({ selectAll, selection });
	};

	isSelected = key => {
		return this.state.selection.includes(key);
	};

	removeSelectedItem = () => {
		let items = this.state.data;
		for (let i = 0; i < items.length; i++) {
			let indexToDelete = -1
			for(let j = 0; j < this.state.selection.length; j++) {
				if (items[i]._id === this.state.selection[j]) {
					indexToDelete = i
				}
				if (indexToDelete !== -1) {
					items.splice(indexToDelete, 1)
					this.setState({ data : items, selection: [] });
				}
			}
		}
	};

	render() {
		const { toggleSelection, toggleAll, isSelected, removeSelectedItem } = this;
		const { data, columns, selectAll } = this.state;
		const checkboxProps = {
			selectAll,
			isSelected,
			toggleSelection,
			toggleAll,
			selectType: "checkbox",
			getTrProps: (s, r) => {
				if (!r) { return {};} else {
				const selected = this.isSelected(r.original._id);
				return {
					style: {
						backgroundColor: selected ? "lightgreen" : "inherit"
					}
				};
			}
			}
		};

		const enabled = this.state.selection.length > 0;
		
		return (
			
			<div className="Table">
				<i>Home</i>
				<h2>Person list</h2>
				<CheckboxTable
					ref={r => (this.checkboxTable = r)}
					data={data}
					filterable
					defaultFilterMethod={(filter, row) =>
					String(row[filter.id]) === filter.value}
					columns={[
						{
							Header: "Name",
							columns: [
								{
									Header: "First Name",
									accessor: "firstName",
									filterMethod: (filter, rows) =>
										matchSorter(rows, filter.value, { keys: ["firstName"] }),
									filterAll: true
								},
								{
									Header: "Last Name",
									id: "lastName",
									accessor: d => d.lastName,
									filterMethod: (filter, rows) =>
										matchSorter(rows, filter.value, { keys: ["lastName"] }),
									filterAll: true
								}
							]
						},
						{
							Header: "Info",
							columns: [
								{
									Header: "Age",
									accessor: "age"
								},
								{
									Header: "Over 21",
									accessor: "age",
									id: "over",
									Cell: ({ value }) => (value >= 21 ? "Yes" : "No"),
									filterMethod: (filter, row) => {
										if (filter.value === "all") {
											return true;
										}
										if (filter.value === "true") {
											return row[filter.id] >= 21;
										}
										return row[filter.id] < 21;
									},
									Filter: ({ filter, onChange }) =>
										<select
											onChange={event => onChange(event.target.value)}
											style={{ width: "100%" }}
											value={filter ? filter.value : "all"}
										>
											<option value="all">Show All</option>
											<option value="true">Can Drink</option>
											<option value="false">Can't Drink</option>
										</select>
								}
							]
						}
					]}
					defaultPageSize={10}
					className="-striped -highlight"
					{...checkboxProps}
				/>
				<br />
				<button onClick={removeSelectedItem} className="btn btn-danger RemoveSelectedItem" disabled={!enabled}>Remove selected item(s)</button>
			</div>
		);
	}
}

export default Table