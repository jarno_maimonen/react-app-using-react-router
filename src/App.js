import React, { Component } from "react";
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import { makeData } from "./utils/Utils";
import Chance from "chance";
import Table from "./components/Table"
import Tools from "./components/Tools"
import './App.css';

const chance = new Chance();

function getData(testData) {
  const data = testData.map(item => {
    const _id = chance.guid();
    return {
     _id,
      ...item
    };
  });
  return data;
}

class App extends Component {

  constructor() {
    super();
  //  const data = getData(makeData(5));
    this.state = {
      data: []
    };
    this.dataUpdater = this.dataUpdater.bind(this)

  }

  dataUpdater = (data) => {
    let list = Array.from(this.state.data);
    list.push(data);   
    this.setState({data: list});
  }

  componentDidMount() { 
    const data = getData(makeData(5));
    this.setState({ data: data });
  }

  render() {
    return (
      <Router>
        <div>
          <div className="App-header">
            <div className="Home">
              <Link to="/">Home</Link>
            </div>
            <div className="Admin">
              <Link to="/admin">Admin</Link>
            </div>
          </div>

          <Route exact path={"/"} component={() => <Table data={this.state.data} />}/>
          <Route exact path={"/admin"} component={() => <Tools data={this.state.data} dataUpdater={this.dataUpdater} />}/>
        </div>
      </Router>
    )
  }
}

export default App;